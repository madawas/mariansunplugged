function loadPage(link){
    if ($(window).width() > 980){var delay = 600;}
    else {var delay = 0}

    $(".page .wrapper").removeClass("active");
    $(".page .container").stop().delay(delay).queue(function(nxt) {

        $.ajax({
            url: link,
            dataType : "html",
            beforeSend: function(){
                $(".page .loader").addClass("active");
            },
            success: function (data) {
                $(".page .container").html(data);
                $(".page .wrapper").addClass("active");

                $(".page .container .portfolio .group:first").addClass("active");
                $(".page .loader").removeClass("active");

                Resizer();
            }
        });
        nxt();
    });

}

function setPosition(item, angle, fulldiameter){
    var itemDistance = 0.35 * fulldiameter;
    var px = Math.cos(angle)*itemDistance + fulldiameter/2;
    var py = Math.sin(angle)*itemDistance + fulldiameter/2;
    item.css("top", py+"px");
    item.css("left", px+"px");

}

function Regulation(count){
    var num = 0;
    $(".mainmenu .menuitem").each(function(){
        var item = $(this);
        var angle = 360*Math.PI/180/count*num;
        var fulldiameter = $(".mainmenu").width(); //width(height) of menu circle

        setPosition(item, angle, 440);
        num++;
    });
}

function Rotate(item, angle){
    var angledeg = 'rotate(' + angle + 'deg)';
    item.css({
        "-webkit-transform": angledeg,
        "-moz-transform": angledeg,
        "-o-transform": angledeg,
        "-ms-transform": angledeg,
        "transform": angledeg
    });
}

function MakeMenu(itemsCount){
    Regulation(itemsCount);
    Rotate($(".mainmenu #linecontainer .overwrapper"), Math.floor(180 - 360/itemsCount));
}

function Resizer(){
    if ($(window).width() > 980){
    $(".page .wrapper").height($(window).height()); //Fix for FireFox
    }

    if($(".page .container .portfolio .group.active").height() > 100){
    $(".page .container .portfolio .all-items").css("min-height", $(".page .container .portfolio .group.active").outerHeight()+"px");
    } else {
        $(".page .container .portfolio .all-items").css("min-height", $(".page .container").width()-100);
    }
}

function SlideNext(){
    if($(".portfolio .all-items .active").is(":last-child") == false){
        $(".portfolio .all-items .active").next().toggleClass('active');
        $(".portfolio .all-items .active:first").toggleClass('active');
    }
    Resizer();
}

function SlidePrev(){
    if($(".portfolio .all-items .active").is(":first-child") == false){
        $(".portfolio .all-items .active").prev().toggleClass('active');
        $(".portfolio .all-items .active:last").toggleClass('active');
    }
    Resizer();
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));

}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function(){
    var itemsCount = $(".mainmenu .menu-items > .menuitem").length;

    $(".mainmenu .menu-items").on("click", ".menuitem", function(){
        //if ($(window).width() > 980){
            Rotate($(".mainmenu #linecontainer"), (360/$(".mainmenu .menu-items > .menuitem").length/2)+(360/$(".mainmenu .menu-items > .menuitem").length*$(this).index()));
        //}
        $(".mainmenu .menu-items .active").removeClass("active");
        $(this).addClass("active");
        loadPage($(this).attr("href"));

        return false;
    });

    $(".page .container").on("click", "a.innerlink", function(){
        loadPage($(this).attr("href"));

        return false;
    });

    $(".page .container").on("focusin", ".contacts input", function(){
        $(this).siblings("i").addClass("active");
    });
    $(".page .container").on("focusout", ".contacts input", function(){
        $(this).siblings("i").removeClass("active");
    });

    MakeMenu($(".mainmenu .menu-items > .menuitem").length);


    $(".mainmenu .menu-items .menuitem:first").click();

    $(".page .container").on("click", ".portfolio .nright", function(){
        SlideNext();
    });
    $(".page .container").on("click", ".portfolio .nleft", function(){
        SlidePrev();
    });

    Resizer();

    var status = getUrlParameter('status');
    if (status === '0') {
        noty({
            layout: 'topCenter',
            text: 'Your reservation is successful. Please check your mailbox for the confirmation.',
            type: 'success',
            timeout: '5000'
        });
    } else if (status === '-1') {
        noty({
            layout: 'topCenter',
            text: 'Unexpected error occurred. Please try again later. If the problem preserves, please contact us.',
            type: 'error',
            timeout: '5000'
        });
    } else if (status === '1') {
        noty({
            layout: 'topCenter',
            text: 'Your message received successfully. We will contact you soon as possible.',
            type: 'success',
            timeout: '5000'
        });        
    } else if (status === '-2') {
        noty({
            layout: 'topCenter',
            text: 'Captcha cannot be validated. This could be a potential harm. Please do not violate the security policies',
            type: 'error',
            timeout: '5000'
        });
    }

    FlipClock.Lang.Custom = { days:'Days', hours:'Hours', minutes:'Minutes', seconds:'Seconds' };
    var opts = {
        clockFace: 'DailyCounter',
        countdown: true,
        language: 'Custom'
    };
    opts.classes = {
        active: 'flip-clock-active',
        before: 'flip-clock-before',
        divider: 'flip-clock-divider',
        dot: 'flip-clock-dot',
        label: 'flip-clock-label',
        flip: 'flip',
        play: 'play',
        wrapper: 'flip-clock-small-wrapper'
    };
    var countdown = 1481459400 - ((new Date().getTime())/1000); // from: 12/11/2016 06:00 pm +0530
    countdown = Math.max(1, countdown);
    $('#screen-timer').FlipClock(countdown, opts);
});

$(window).resize(function(){
    Resizer();
});