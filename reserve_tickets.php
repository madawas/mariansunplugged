<?php
require 'PHPMailer/PHPMailerAutoload.php';
require 'recaptcha/autoload.php';

$url = 'https://docs.google.com/forms/d/1e3Lk2nn3ZPFVMYyWRKPKwbS4TyUameZWM-LODGTEr6U/formResponse';
$captcha_url = 'https://www.google.com/recaptcha/api/siteverify';
$secret = '6LcazQoUAAAAANBSwm3mud0Aej4mc2g1mIkwRIgw';

//validating captcha
$recaptcha = new \ReCaptcha\ReCaptcha($secret);
$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
if (!$resp->isSuccess()) {
    header("Location: index.htm?status=-2");
} 

$data = array (
    'entry.816303452' => $_POST['name'],
    'entry.190334096' => $_POST['phone'],
    'entry.503904079' => $_POST['email'],
    'entry.1722502203' => $_POST['tickets_number'],
    'entry.178995676' => $_POST['ticket_price'],
    'entry.1246859235' => $_POST['message']
);

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
        'method' => 'POST',
        'content' => http_build_query($data),
    ),
);
$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);
$qrcontent = hash('sha256', http_build_query($data));

$mail = new PHPMailer;

$mail->isSMTP(); // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com'; // Specify main and backup server
$mail->Port = 587;
$mail->SMTPAuth = true; // Enable SMTP authentication
$mail->Username = 'events.awe@gmail.com'; // SMTP username
$mail->Password = 'MandaleUnplugged'; // SMTP password
$mail->SMTPSecure = 'tls'; // Enable encryption, 'ssl' also accepted

$mail->AddReplyTo('events.awe@gmail.com', 'Awe! Events');
$mail->From = 'events.awe@gmail.com';
$mail->FromName = 'Awe! Events';
$mail->addAddress($_POST['email']);

$mail->isHTML(true);
$mail->Subject = 'Awe! Events - Legends Unplugged by Marians';
$mail->Body = 'Dear ' . $_POST['name'] . ', <br/><br/>' .
    'We would like to confirm your reservation of  ' . $_POST['tickets_number'] . ' tickets for Legends Unplugged by ' .
    'Marians Concert.<br/><br/>Please find the reservation information below.<br/><br/>' .
    '<div style="border: dashed; border-radius:25px; padding-left:25px; width: 50%">' .
    '<table><tbody><tr><td><h2><strong>Legends Unplugged by Marians</strong></h2><h3>Reservation Info</h3>' .
    '<p>Name - ' . $_POST['name'] . '</p><p>Contact - ' . $_POST['phone'] . '</p><p>Tickets - LKR ' . $_POST['ticket_price'] .
    ' x ' . $_POST['tickets_number'] . '</p></td>' .
    '<td><img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=' . $qrcontent . '&choe=UTF-8" /></td></tr>' .
    '</tbody></table></div><br/><br/>' .
    '<p><strong>Note: Enable images and print this email and produce when purchasing tickets.</strong></p><br/><br/>' .
    'Keep in touch with our <a href="https://www.facebook.com/MariansUnplugged/">Facebook</a> page for updates.<br/><br/>' .
    'Thanking You,<br/>' .
    'Awe! Events,<br/>';

$mail->AltBody = 'Dear ' . $_POST['name'] . ',' .
    'We would like to confirm your reservation of  ' . $_POST['tickets_number'] . ' for Legends Unplugged by Marians' .
    'Concert. <br/><br/>' .
    'Please find the reservation information below.' .
    'Name - ' . $_POST['name'] .
    'Contact - ' . $_POST['phone'] .
    'Tickets - LKR ' . $_POST['ticket_price'] . ' x ' . $_POST['tickets_number'] .
    'Note: Print this email and produce when purchasing tickets.' .
    'Keep in touch with our <a href="https://www.facebook.com/MariansUnplugged/">Facebook</a> page for updates.<br/><br/>' .
    'Thanking You,' .
    'Awe! Events,';

if (!$mail->send()) {
    header("location:index.htm?status=-1");
}

header("Location: index.htm?status=0");